#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <dji_sdk/Gimbal.h>
#include "mbzirc_mission/mission_modes.h"


namespace ca {

  class GimbalControl {
    
      DJIDrone* drone_;
      double kp_;
      int y_target_;
      dji_sdk::Gimbal curr_gimbal_pos_;
      ros::NodeHandle n_;
      int curr_y_;
      ros::Time time_got_rect_;
      double gimbal_angle_;
    
  public:
    
    GimbalControl(ros::NodeHandle n, DJIDrone* drone, int y_target);
    void gimbalCallback(const dji_sdk::Gimbal::ConstPtr &msg);
    void gimbalFixedCallback(const dji_sdk::Gimbal::ConstPtr &msg);
    void fixGimbalAngle( double gimbal_angle);
    void setY(ros::Time t, int y);
    void setPitch(double pitch);
    void saturatePitch();
    void controlPitch();
  };
 
}
