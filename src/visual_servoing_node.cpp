#include <ros/ros.h>
#include <dji_sdk/Gimbal.h>
#include <dji_sdk/FlightControlInfo.h>
#include <tf/transform_broadcaster.h>
#include <opencv_apps/RotatedRect.h>
#include <math.h>
#include "mbzirc_mission/mission_modes.h"
//#include "mbzirc_visualservoing/GimbalControl.h"
#include "mbzirc_visualservoing/BodyControlVISP.h"
#include "blink1/Blink1.h"
#include <message_filters/subscriber.h>


bool alreadyAuto = false;
DJIDrone* drone;
ca::GimbalControl* Gimbal;
ca::BodyControl* Body;
ros::Time time_got_rect;


void targetCallback(const opencv_apps::RotatedRectStamped::ConstPtr &msg){
   opencv_apps::RotatedRectStamped curr_rect;
   curr_rect=(*msg);
   time_got_rect = ros::Time::now();
   Gimbal->setY(time_got_rect, curr_rect.rect.center.y);
   Body->setRect(time_got_rect, curr_rect);
}  


int main(int argc, char** argv){
  ros::init(argc, argv, "visual_servoing");

  ros::NodeHandle n;
    
  drone = new DJIDrone(n); 
  
  int x_target, y_target, y_offset, rect_width, rect_height;
  double target_height, rect_angle;
  double gimbal_angle;  // the fixed gimbal angle during visual servoing
  
  // The default parameters were the ones tested with the real robot in the highbay
  n.param("/visual_servoing/x_target", x_target, 320);
  n.param("/visual_servoing/y_target", y_target, 180); 
  n.param("/visual_servoing/y_offset", y_offset, -60); // -60 pixels is an offset to make the drone in the center of the deck
  n.param("/visual_servoing/target_height", target_height, 1.0); // Height from the ground
  n.param("/visual_servoing/rect_width", rect_width, 360);
  n.param("/visual_servoing/rect_height", rect_height, 360);
  n.param("/visual_servoing/rect_angle", rect_angle, M_PI/2);
  n.param("/visual_servoing/gimbal_angle", gimbal_angle, -45.0); // for fixing the gimbal angle during visual servoing
  
  opencv_apps::RotatedRectStamped target_rect;
  target_rect.rect.center.x=x_target; 
  target_rect.rect.center.y=y_target+y_offset;
  target_rect.rect.size.width=rect_width;
  target_rect.rect.size.height=rect_height;
  target_rect.rect.angle=rect_angle; 
  
  Gimbal = new ca::GimbalControl(n, drone, y_target);  // Control the gimbal pitch only
  
  Body = new ca::BodyControl(n, target_rect, target_height); // Control x, y, z and heading
  
  Gimbal->fixGimbalAngle(gimbal_angle);    // setting the gimbal fixed angle

  // NOTE: to use non-fixed gimbal angle call gimbalCallback, to use fixed gimbal angle call gimbalFixedCallback
  ros::Subscriber sub1 = n.subscribe("/dji_sdk/gimbal", 1, &ca::GimbalControl::gimbalCallback, Gimbal); 
  ros::Subscriber sub2 = n.subscribe("/dji_sdk/odometry", 1, &ca::BodyControl::odometryCallback, Body);
  ros::Subscriber sub3 = n.subscribe("/dji_sdk/camera_info", 1, &ca::BodyControl::cameraInfoCallback, Body);
  ros::Subscriber sub4 = n.subscribe("/deck_track/target", 1, &targetCallback);
  
  bool hasBlink;
  n.param("/visual_servoing/hasBlink", hasBlink, true);
  
  Blink1* led;
  if (hasBlink) led = new Blink1(n);
  ros::Duration(2.0).sleep();
  if (hasBlink) led->blink(0, 0, 255, 300); // Blinking Blue
  
  ros::Rate loop_rate(15);
  time_got_rect = ros::Time::now();
  ros::Duration(4.0).sleep(); 
 
  Body->init();
   

  while (ros::ok())
  {

        ros::spinOnce();
       
	n.getParamCached("/mission/authorized", alreadyAuto);
       
       	ros::Duration time_last_rectangle = ros::Time::now()-time_got_rect; 
       	if (time_last_rectangle.toSec()<0.5) {              
	    if (alreadyAuto){
		Body->control();
		if (hasBlink) led->fade(0,255,0,100); // Green	
	    }
	    else{
                if (hasBlink) led->blink(0, 255, 0, 300); // Blinking Green
	    }
	}
       	else {
	    if (alreadyAuto){
		// it's been more than 0.5s since the last image, move only using feed forward
	        Body->controlFF();
	        if (hasBlink) led->fade(255,0,0,100); // Red
	    }
	    else{
               if (hasBlink) led->blink(255, 0, 0, 300); // Blinking Red
	    }
	}
	
     
         loop_rate.sleep();  
    	       
  }


  return 0;
}
