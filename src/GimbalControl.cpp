#include <mbzirc_visualservoing/GimbalControl.h>

namespace ca {

 
GimbalControl::GimbalControl(ros::NodeHandle n, DJIDrone* drone, int y_target) : n_(n), drone_(drone), y_target_(y_target) {
       n_.param("/visual_servoing/kp_gimbal", kp_, 2.0);
       ROS_INFO("Visual Servoing - kp_gimbal=%f", kp_);
       drone_->gimbal_angle_control(0, 0, 0, 20, 1, 1, 1, 0);
       setPitch(-45.0);
    }
    
void GimbalControl::fixGimbalAngle( double gimbal_angle){
  gimbal_angle_ = gimbal_angle;
}

void GimbalControl::gimbalCallback(const dji_sdk::Gimbal::ConstPtr &msg){ 
                curr_gimbal_pos_=(*msg);
		
		int mode = IDLE; 
		
		n_.getParamCached("/mission/mode", mode);
		
		if (mode & GIMBALDOWN){
		    setPitch(-84.0);
		}
		else if (mode & GIMBALTRACK){
	 
		      saturatePitch();
	        
		      ros::Duration time_last_rectangle = ros::Time::now()-time_got_rect_; 
		      if (time_last_rectangle.toSec()<0.5){                  // Track only if it is a new message
			       controlPitch();
            
		      }
		      else if (time_last_rectangle.toSec()>1.5){ // if did not receive a new message, go to -45 degrees
			       setPitch(gimbal_angle_);
		      }
		}
		
		
    }  

void GimbalControl::gimbalFixedCallback(const dji_sdk::Gimbal::ConstPtr &msg){ 
                curr_gimbal_pos_=(*msg);
    
    int mode = IDLE; 
    
    n_.getParamCached("/mission/mode", mode);
    
    if (mode & GIMBALDOWN){
        setPitch(-84.0);
    }
    else if (mode & GIMBALTRACK){
   
          saturatePitch();
          
          ros::Duration time_last_rectangle = ros::Time::now()-time_got_rect_; 
          setPitch(gimbal_angle_);

    }
    
    
    }  
    
void GimbalControl::setY(ros::Time t, int y){
      time_got_rect_=t;
      curr_y_=y;
    }
    
void GimbalControl::setPitch(double pitch){
      if ((curr_gimbal_pos_.pitch>(pitch+1.0)) || (curr_gimbal_pos_.pitch<(pitch-1.0)))	
		drone_->gimbal_angle_control(0, pitch*10, 0, 10, 1, 1, 1, 0);  // roll, pitch, yaw, duration, absolute, ignore_yaw, ignore_roll, do_not_ignore_pitch
    }

void GimbalControl::saturatePitch(){
      if (curr_gimbal_pos_.pitch>0)       // saturates pitch in 0 ...                
          drone_->gimbal_angle_control(0, 0, 0, 10, 1, 1, 1, 0); 
      if (curr_gimbal_pos_.pitch<-88)     // ... 88 degrees. 
          drone_->gimbal_angle_control(0, -880, 0, 10, 1, 1, 1, 0); 
    }
    
void GimbalControl::controlPitch(){

      n_.getParamCached("/visual_servoing/kp_gimbal", kp_);

      double vy = kp_*(y_target_ - curr_y_);
      
      if ( ((curr_gimbal_pos_.pitch>=0) && (vy<0)) || ((curr_gimbal_pos_.pitch<=-88) && (vy>0)) || ((curr_gimbal_pos_.pitch<0) && (curr_gimbal_pos_.pitch>-88)) ){
      	    drone_->gimbal_speed_control(0, vy, 0);
                   //ROS_INFO("%f %f",curr_gimbal_pos.pitch, vy); 
      }
      
    }

}
