#ifndef _BodyControlVISP_

#define _BodyControlVISP_

#include <ros/ros.h>
//#include <dji_sdk/dji_drone.h>
#include <nav_msgs/Odometry.h>
#include <opencv_apps/RotatedRectStamped.h>
#include <image_geometry/pinhole_camera_model.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/CameraInfo.h>
#include <tf/transform_listener.h>
#include "mbzirc_visualservoing/GimbalControl.h"
#include <visp/vpFeatureBuilder.h>
#include <visp/vpServo.h>
#include <visp/vpSimulatorCamera.h>
#include <visp_bridge/camera.h>
#include <visp_bridge/3dpose.h>
#include <mbzirc_commons/tools/logger.h>
#include <mbzirc_commons/classes/system.h>
#include <mbzirc_commons/tools/filters.h>
#include <geometry_msgs/TwistStamped.h>
#include "mbzirc_mission/mission_modes.h"


namespace ca {

  class BodyControl {
    
      double kpg_, kph_, lambda_, kpheight_, ff_vx_, ff_vy_, kpxy_, kpy_, ff_vz_body;
      opencv_apps::RotatedRectStamped target_rect_;
      double desired_height_;                               //  this is the desired height for the quad above the deck
      double deck_height_;                 	            //  this the height from the ground     
      opencv_apps::RotatedRectStamped curr_rect_;
      opencv_apps::RotatedRectStamped prev_rect_;
      nav_msgs::Odometry curr_odometry_;
      ros::NodeHandle n_;
      ros::Time time_got_rect_;
      ros::Time time_got_prev_rect_;
      vpServo task_ ;
      vpFeaturePoint p_[5], pd_[5] ;
      vpCameraParameters cam_;   // Intrinsic camera parameters
      image_geometry::PinholeCameraModel cam_model_;
      bool got_cam_info_, init_;
      tf::TransformListener tf_listener_;
      int mode_;
      //mbzirc::commons::MedianFilter *filterVx_, *filterVy_;
      mbzirc::commons::Logger::ILogger vel_log;
      long int seq_;
      ros::Publisher twist_command_pub_;
      geometry_msgs::Vector3Stamped  ff_velocity_vector_body_;
      vpColVector v_, e_;
      bool vInitialized_;
      double k_accelerate_;
  public:
    
     BodyControl(ros::NodeHandle n, opencv_apps::RotatedRectStamped target_rect, double height);
     ~BodyControl();
     bool init();
     void cameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& cam_info);
     void odometryCallback(const nav_msgs::OdometryConstPtr &msg);
     void setRect(ros::Time t, opencv_apps::RotatedRectStamped curr_rect);
     //geometry_msgs::PointStamped computeRectangleCenterWorld(opencv_apps::RotatedRectStamped rectangle);
     //void estimateTargetVelocity();
     void control();
     void controlFF();
     //void resetVelocityFilter();

      
  };

}

#endif
