#include <mbzirc_visualservoing/BodyControlVISP.h>
  

namespace ca {

   
BodyControl::BodyControl(ros::NodeHandle n, opencv_apps::RotatedRectStamped target_rect, double height) : n_(n), target_rect_(target_rect), desired_height_(height){
      n_.param("/visual_servoing/kp_gimbal", kpg_, 2.5);
      n_.param("/visual_servoing/kp_heading", kph_, 0.1); 
      n_.param("/visual_servoing/lambda", lambda_, 0.5);
      n_.param("/visual_servoing/kp_height", kpheight_, 0.9);
      n_.param("/visual_servoing/kp_xy", kpxy_, 1.0);
      n_.param("/visual_servoing/kp_y", kpy_, 0.8);
      
      n_.param("/visual_servoing/ff_vx", ff_vx_, 0.0);
      n_.param("/visual_servoing/ff_vy", ff_vy_, 0.0);
      n_.param("/visual_servoing/ff_vy", ff_vz_body, 0.0);
      
      n_.param("/visual_servoing/k_accelerate", k_accelerate_, 1.0);
      
      twist_command_pub_ = n_.advertise<geometry_msgs::TwistStamped>("/command/twist", 1);
      
      ROS_INFO("Visual Servoing - kp_gimbal=%f, kp_heading=%f, lambda=%f, kp_height=%f", kpg_, kph_, lambda_, kpheight_);
      init_= false;
      got_cam_info_=false; 
      mode_=IDLE;
      seq_=0;
  
      int isLogEnabled;
      if (n.getParam("/mbzirc/log", isLogEnabled) == false)
	ROS_INFO("Visual Servoing: Parameter '/mbzirc/log' is not defined! Logging is disabled!");
      else if (isLogEnabled > 0) 
      {
	ILOG_INIT_SIMPLE_NUMERIC_UNIQUE(vel_log, mbzirc::commons::System::GetHomeDir() + "/logs/visualservoing_vel");
	LOG_INIT_SIMPLE_NUMERIC_UNIQUE(mbzirc::commons::System::GetHomeDir() + "/logs/visualservoing");
      }
    }
    
BodyControl::~BodyControl(){
      task_.kill();   
    }
    
bool BodyControl::init(){
       
      while (!got_cam_info_ && ros::ok()){
	ros::spinOnce();
        ROS_INFO("Visual Servoing: Waiting for camera info...");
	ros::Duration(0.5).sleep();
      }
      if (!got_cam_info_){
        ROS_ERROR("Visual Servoing: Never got camera info!");
	return false;
      }
      else ROS_INFO("Visual Servoing: Got camera info...");
      	
      task_.setServo(vpServo::EYEINHAND_L_cVe_eJe  );
      task_.setInteractionMatrixType(vpServo::DESIRED); // The interaction Matrix will use a constant Z determined by Heigth
      task_.setLambda(lambda_);
      
      vpMatrix eJe(6,4); // Computing only vx, vy, vz and yawrate
      eJe[0][0]=1; //vx
      eJe[1][1]=1; //vy
      eJe[2][2]=1; //vz
      eJe[5][3]=1; //yawrate
      //std::cout << "eJe:\n" << eJe << std::endl;
      task_.set_eJe(eJe);
      
      vpImagePoint iP[5];       // the points in the image
      
      iP[0].set_ij(target_rect_.rect.center.y, target_rect_.rect.center.x); // Pixel coordinates of desired patern 
      
      iP[1].set_ij(target_rect_.rect.center.y+target_rect_.rect.size.height/2*cos(target_rect_.rect.angle), target_rect_.rect.center.x+target_rect_.rect.size.height/2*sin(target_rect_.rect.angle));
      
      iP[2].set_ij(target_rect_.rect.center.y-target_rect_.rect.size.width/2*sin(target_rect_.rect.angle),target_rect_.rect.center.x+target_rect_.rect.size.width/2*cos(target_rect_.rect.angle));
      
      iP[3].set_ij(target_rect_.rect.center.y-target_rect_.rect.size.height/2*cos(target_rect_.rect.angle),target_rect_.rect.center.x-target_rect_.rect.size.height/2*sin(target_rect_.rect.angle));    
      
      iP[4].set_ij(target_rect_.rect.center.y+target_rect_.rect.size.width/2*sin(target_rect_.rect.angle),target_rect_.rect.center.x-target_rect_.rect.size.width/2*cos(target_rect_.rect.angle));
       
      //ROS_INFO("(%f %f) (%f %f) (%f %f) (%f %f) (%f %f)", iP[0].get_i(), iP[0].get_j(), iP[1].get_i(), iP[1].get_j(), iP[2].get_i(), iP[2].get_j(), iP[3].get_i(), iP[3].get_j(), iP[4].get_i(), iP[4].get_j() );
      
      // Initialize the desired feature points
      for (unsigned int i = 0 ; i < 5 ; i++) {
	vpFeatureBuilder::create(pd_[i], cam_, iP[i]); // create feature points -> x, y are points in the image frame given in meter, Z is the depth of the point 
	pd_[i].set_Z(desired_height_);
        vpFeatureBuilder::create(p_[i], cam_, iP[i]); // p_ is the current feature. I'm initializing with pd_[i];
	p_[i].set_Z(desired_height_);
	task_.addFeature(p_[i], pd_[i]); // Adding each feature to the task. This will create an Interaction Matrix that is 10x6
      }  

      vInitialized_=false;
      
      ff_velocity_vector_body_.vector.x=ff_velocity_vector_body_.vector.y=ff_velocity_vector_body_.vector.z=0;
      
      // This is here to initialize the cache. The first call is slower than the others.
      n_.getParamCached("/mission/mode", mode_); 
      n_.getParamCached("/visual_servoing/kp_height", kpheight_);
      n_.getParamCached("/visual_servoing/kp_xy", kpxy_);
      n_.getParamCached("/visual_servoing/kp_y", kpy_);
      n_.getParamCached("/visual_servoing/kp_heading", kph_);
      n_.getParamCached("/visual_servoing/lambda", lambda_);

      n_.getParamCached("/visual_servoing/ff_vx", ff_vx_); 
      n_.getParamCached("/visual_servoing/ff_vy", ff_vy_); 
      n_.getParamCached("/visual_servoing/ff_vz_body", ff_vz_body); 
      
      bool accelerate=false;
      n_.getParamCached("/mission/accelerate", accelerate); 
      
      bool decreaseHeight = true;
      n_.getParamCached("/mission/decreaseHeight", decreaseHeight); 
        
      init_=true;
      prev_rect_.header.seq=0;
      return true;
    }
    
void BodyControl::cameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& cam_info){
                
                cam_ = visp_bridge::toVispCameraParameters(*cam_info);
		cam_model_.fromCameraInfo(cam_info);
                got_cam_info_ = true;
        }
    
    
void BodyControl::odometryCallback(const nav_msgs::OdometryConstPtr &msg){ 
                curr_odometry_ = (*msg);

    }  
       
    
void BodyControl::setRect(ros::Time t, opencv_apps::RotatedRectStamped curr_rect)
    {
       time_got_rect_=t;
       curr_rect_=curr_rect;
    }
        
         
        
void BodyControl::control(){
  
      if (init_){
		  
	n_.getParamCached("/mission/mode", mode_);
		    
	if (mode_ & TRACK){
	    
	  try {

	    // Create the points given the rectangle
	    vpImagePoint iP[5];       // the points in the image
	  
	    iP[0].set_ij(curr_rect_.rect.center.y,curr_rect_.rect.center.x); // Pixel coordinates of current patern  
	    iP[1].set_ij(curr_rect_.rect.center.y+curr_rect_.rect.size.height/2*cos(curr_rect_.rect.angle), curr_rect_.rect.center.x+curr_rect_.rect.size.height/2*sin(curr_rect_.rect.angle));
	    iP[2].set_ij(curr_rect_.rect.center.y-curr_rect_.rect.size.width/2*sin(curr_rect_.rect.angle), curr_rect_.rect.center.x+curr_rect_.rect.size.width/2*cos(curr_rect_.rect.angle));   
	    iP[3].set_ij(curr_rect_.rect.center.y-curr_rect_.rect.size.height/2*cos(curr_rect_.rect.angle), curr_rect_.rect.center.x-curr_rect_.rect.size.height/2*sin(curr_rect_.rect.angle));
	    iP[4].set_ij(curr_rect_.rect.center.y+curr_rect_.rect.size.width/2*sin(curr_rect_.rect.angle), curr_rect_.rect.center.x-curr_rect_.rect.size.width/2*cos(curr_rect_.rect.angle));        
            
	    //ROS_INFO("Rect %f %f %f %f %f", curr_rect_.rect.center.x, curr_rect_.rect.center.y, curr_rect_.rect.center.x-curr_rect_.rect.size.height, curr_rect_.rect.center.x-curr_rect_.rect.size.width, curr_rect_.rect.angle);
	    
	    for (unsigned int i = 0 ; i < 5 ; i++) {
		vpFeatureBuilder::create(p_[i], cam_, iP[i]); // create feature points -> x, y are points in the image frame given in meter, Z is the depth of the point 
		//p_[i].set_Z(1.0); // Z is not necessary because I'm constructing the iteraction matrix with a constant Z
	    }
	    n_.getParamCached("/visual_servoing/kp_height", kpheight_);
	    n_.getParamCached("/visual_servoing/kp_xy", kpxy_);
	    n_.getParamCached("/visual_servoing/kp_y", kpy_);
	    n_.getParamCached("/visual_servoing/kp_heading", kph_);
	    n_.getParamCached("/visual_servoing/lambda", lambda_);
	    
	    try {
	      tf::StampedTransform transform;
	      tf_listener_.lookupTransform("/camera_visp", "/base_frame", curr_rect_.header.stamp, transform); // This transformation includes gimbal and camera_visp

	      geometry_msgs::Transform transf;
	    
	      quaternionTFToMsg(transform.getRotation(),transf.rotation);
	      vector3TFToMsg(transform.getOrigin(),transf.translation);

	      vpHomogeneousMatrix cMf=visp_bridge::toVispHomogeneousMatrix(transf);
	      task_.set_cVe(cMf);  // Set the transformation matrix between the body and the camera
     	    
	      task_.setLambda(lambda_);
	      v_ = task_.computeControlLaw();
	      e_ = task_.getError(); 
	      vInitialized_=true;
	     }
	    catch(tf2::ExtrapolationException& ee){
	       ROS_ERROR("Transform camera_visp-> base_frame  exception: ");
	    } 		    
	     
	    try { 
	      /////////////////////////////////////////////////////////////////////////////////////////////////////////////
	      // feedforward - feedforward is necessary to track the robot. 
	    
	      n_.getParamCached("/visual_servoing/ff_vx", ff_vx_); //  feedforward vx
	      n_.getParamCached("/visual_servoing/ff_vy", ff_vy_); //  feedforward vy
	    
	      geometry_msgs::Vector3Stamped  ff_velocity_vector;
           
	      // Create a stamped vector to be transformed to the body 
	      ff_velocity_vector.header.frame_id = "/ned"; // For simulation this should be world
	      ff_velocity_vector.header.stamp = curr_rect_.header.stamp; 
	      ff_velocity_vector.vector.x = ff_vx_;
	      ff_velocity_vector.vector.y = ff_vy_;
	      ff_velocity_vector.vector.z = 0.0; 
              tf_listener_.transformVector("/base_frame", ff_velocity_vector, ff_velocity_vector_body_);
	      ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	      //   using namespace DJI::onboardSDK;
	      //   uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
	      //	    | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_BODY | Flight::YawCoordinate::YAW_BODY;
	      //    drone_->attitude_control(flag, v[0]+ff_vx_, v[1], -kpheight_*v[2], kph_*180*v[3]/M_PI);  
	     
	      geometry_msgs::TwistStamped twist;
	      if (vInitialized_){    // This is to prevent crashing when we cannot get the transform in the first iteraction. 	  
		  LOG_INFO("%f %f %f %f %f %f %f %f %f %f %f %f %f %f", v_[0]+ff_velocity_vector_body_.vector.x, v_[1]+ff_velocity_vector_body_.vector.y, v_[2]+ff_velocity_vector_body_.vector.z, v_[3], e_[0], e_[1], e_[2], e_[3], e_[4], e_[5], e_[6], e_[7], e_[8], e_[9]);
		  twist.header.frame_id = "/base_frame";
		  twist.header.stamp = ros::Time::now();
		  twist.header.seq = seq_++;
		  twist.twist.linear.x = kpxy_*v_[0]+ff_velocity_vector_body_.vector.x;
		  twist.twist.linear.y = kpy_*v_[1]+ff_velocity_vector_body_.vector.y;
		  // twist.twist.linear.z = kpheight_*(v_[2]+ff_velocity_vector_body_.vector.z);
		  twist.twist.linear.z = kpheight_*(v_[2]+0); //hack
		  twist.twist.angular.x=twist.twist.angular.y=0.0;
		  twist.twist.angular.z=kph_*v_[3];
	      }
	      else {
		  twist.header.frame_id = "/base_frame";
		  twist.header.stamp = ros::Time::now();
		  twist.header.seq = seq_++;
		  twist.twist.linear.x = 1*ff_velocity_vector_body_.vector.x;
		  twist.twist.linear.y = 1*ff_velocity_vector_body_.vector.y;
		  // twist.twist.linear.z = kpheight_*(ff_velocity_vector_body_.vector.z);
		  twist.twist.linear.z = 0;
		  twist.twist.angular.x=twist.twist.angular.y=0.0;
		  twist.twist.angular.z=0.0;
	      }
	      ROS_INFO("Inside BodyControl: 1->3 base frame ffX=%f ffY=%f ffz=%f",twist.twist.linear.x, twist.twist.linear.y, twist.twist.linear.z);
	      twist_command_pub_.publish(twist);
	    }
	    catch(tf2::ExtrapolationException& ee){
	     	ROS_ERROR("Transform ned->base_frame exception: ");	 
	    }
	    
	  }
	  catch(vpException ee){ 
	     ROS_ERROR("VisualServoing Catch an exception: ");
	  }
	  
	} //if mode_
      } //if init
      
    } // Control



    void BodyControl::controlFF(){
  
      if (init_){
		  
	n_.getParamCached("/mission/mode", mode_);
		    
	if (mode_ & TRACK){
	    
	  try {
	     
	      /////////////////////////////////////////////////////////////////////////////////////////////////////////////
	      // feedforward - feedforward is necessary to track the robot. 
	    
	      n_.getParamCached("/visual_servoing/ff_vx", ff_vx_); //  feedforward vx
	      n_.getParamCached("/visual_servoing/ff_vy", ff_vy_); //  feedforward vy
	      n_.getParamCached("/visual_servoing/ff_vz_body", ff_vz_body); //  feedforward vz, hard-coded to make it go down
	      
	      // This is to accelerate the drone in the beggining
	      bool accelerate=false;
	      n_.getParamCached("/mission/accelerate", accelerate); 
	      
	      bool decreaseHeight=true;
	      n_.getParamCached("/mission/decreaseHeight", decreaseHeight); 

          if (accelerate){
		ff_vx_=k_accelerate_*ff_vx_;
		ff_vy_=k_accelerate_*ff_vy_;
	      }
	    
	      geometry_msgs::Vector3Stamped  ff_velocity_vector;
           
	      // Create a stamped vector to be transformed to the body 
	      ff_velocity_vector.header.frame_id = "/ned"; 		// For simulation this should be world
	      ff_velocity_vector.header.stamp = ros::Time::now();
	      ff_velocity_vector.vector.x = ff_vx_;
	      ff_velocity_vector.vector.y = ff_vy_;
	      ff_velocity_vector.vector.z = 0.0; 
	      tf_listener_.waitForTransform("/base_frame", "/ned", ff_velocity_vector.header.stamp, ros::Duration(0.2));
	      tf_listener_.transformVector("/base_frame", ff_velocity_vector, ff_velocity_vector_body_);
	      ///////////////////////////////////////////////////////////////////////////////////////////////////////////
          ROS_INFO("Inside BodyControlFF: 1->3 world frame ffX=%f ffY=%f",ff_vx_, ff_vy_);
	      geometry_msgs::TwistStamped twist;
	      twist.header.frame_id = "/base_frame";
	      twist.header.stamp = ros::Time::now();
	      twist.header.seq = seq_++;
	      twist.twist.linear.x = (ff_velocity_vector_body_.vector.x);
	      twist.twist.linear.y = (ff_velocity_vector_body_.vector.y);
	      // twist.twist.linear.z = (ff_velocity_vector_body_.vector.z);
	      
            if (decreaseHeight == true)
                twist.twist.linear.z = ff_vz_body;
            else
                twist.twist.linear.z = 0;
              
	      twist.twist.angular.x=twist.twist.angular.y=0.0;
	      twist.twist.angular.z=0.0;
	      twist_command_pub_.publish(twist);
	      ROS_INFO("Inside BodyControlFF: 1->3 base frame ffX=%f ffY=%f ffz=%f",twist.twist.linear.x, twist.twist.linear.y, twist.twist.linear.z);
              
		
	    }
	    catch(tf2::ExtrapolationException& ee){
	     	ROS_ERROR("Transform ned->base_frame exception in ff %s",ee.what());	 
	    }
	    
	 
	  
	} //if mode_
      } //if init
      
    } // Control

   

}

